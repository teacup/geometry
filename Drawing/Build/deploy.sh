# This requires butler
# https://itch.io/docs/butler/installing.html
# On macOS the butler download is broken. Install the
# itch app instead; then you must run the itch app before
# you can use butler.
set -euxo pipefail

app="Nyan3D"
user="eelstork"

# Zip everything
# Note: when uploading manually to itch.io you don't need
# to compress .app files (macOS) but when uploading via Butler,
# some server side things misinterprets the .app as a zip archive, which
# breaks it.
zip -r $app-Linux $app-Linux
zip -r $app-Windows $app-Windows
zip -r $app.app $app-macOS
# Push with Butler
export PATH=~/Library/Application\ Support/itch/bin:$PATH
butler push "$app-macOS" $user/$app:osx
butler push "$app-Windows.zip" $user/$app:win
butler push "$app-Linux.zip" $user/$app:linux
