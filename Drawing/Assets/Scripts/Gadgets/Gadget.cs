﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gadget : MonoBehaviour {

	public bool collider {
		set { foreach (var c in GetComponentsInChildren<Collider>()) c.enabled = value; }
	}

	public bool renderable{
		set{ foreach (var c in GetComponentsInChildren<Renderer>()) c.enabled = value; } 
	}

}
