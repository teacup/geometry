﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eraser : MonoBehaviour {

	public GameObject selection;
	public Material eraserMaterial;
	public Material selectionMaterial;

	void Start(){ transform.localScale = Vector3.one * 0.04f; }

	void Update () {
		if (Input.GetMouseButtonDown (0)) Destroy (selection);
		var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		bool didHit = Physics.Raycast (ray, out hit);
		if (didHit) {
			if (hit.collider.gameObject == selection)return;
			restoreMaterial();
			if (Selectable (hit.collider.gameObject)) {
				selection = hit.collider.gameObject;
				highlightSelection ();
			} else ClearSelection ();
			
		} else ClearSelection ();
	}

	void ClearSelection(){
		if (selection == null) return;
		selectionRenderer.material = selectionMaterial;
		selectionMaterial = null;
		selection = null;
	}

	bool Selectable(GameObject obj){
		return obj.CompareTag ("Data");
	}

	void highlightSelection(){
		selectionMaterial = selectionRenderer.material;
		selectionRenderer.material = eraserMaterial;
	}

	void restoreMaterial(){
		if (selection != null) selectionRenderer.material = selectionMaterial;
	}

	Renderer selectionRenderer{
		get{ 
			if (selection == null) return null;
			return selection.GetComponent<Renderer> ();
		}
	}

	void OnEnable(){
		GetComponent<Renderer> ().enabled = true;
	}

	void OnDisable(){
		GetComponent<Renderer> ().enabled = false;
	}

}
