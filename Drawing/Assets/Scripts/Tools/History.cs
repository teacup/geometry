﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class History : MonoBehaviour {

	List<AbstractAction> history = new List<AbstractAction>();
	int pointer = -1;

	void Update () {
		if (   Input.GetKey (KeyCode.LeftApple)  || Input.GetKey(KeyCode.RightApple)
			|| Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)
		) {
			if (Input.GetKeyDown (KeyCode.Z)) Undo ();
			if (Input.GetKeyDown (KeyCode.Y)) Redo ();
		}
	}

	// PUBLIC -------------------------------------------------------

	public void Append(AbstractAction x){
		// if we have undoed several actions, they are now discarded
		// as we 'fork' to a new branch.
		while (history.Count > pointer + 1) {
			// TODO undoed actions removed from the stack are not
			// redoable so we can release resources.
			history.RemoveAt (history.Count - 1);
		}
		history.Add		 (x);
		pointer++;
	}

	// PRIVATE -------------------------------------------------------

	void Undo(){
		if (history.Count == 0) {
			print ("History is empty");
			return;
		} if (pointer < 0) {
			print ("Nothing to undo");
			return;
		}
		history [pointer--].Undo ();
	}

	void Redo(){
		if (history.Count == 0) {
			print ("History is empty");
			return;
		} if (pointer+1 >= history.Count) {
			print ("Nothing to redo");
			return;
		}
		history [++pointer].Redo ();
	}

}
