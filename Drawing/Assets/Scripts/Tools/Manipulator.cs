﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *
 */
public class Manipulator : MonoBehaviour {

	const KeyCode ROTATE_LEFT  = KeyCode.Keypad4;
	const KeyCode ROTATE_RIGHT = KeyCode.Keypad6;
	const KeyCode ROTATE_UP    = KeyCode.Keypad8;
	const KeyCode ROTATE_DOWN  = KeyCode.Keypad2;
	public Transform target;

	//float gain=5f;

	void FixedUpdate(){

		if (Input.GetKey (ROTATE_LEFT))  Rotate (-1f,  0f);
		if (Input.GetKey (ROTATE_RIGHT)) Rotate ( 1f,  0f);
		if (Input.GetKey (ROTATE_UP))    Rotate ( 0f,  1f);
		if (Input.GetKey (ROTATE_DOWN))  Rotate ( 0f, -1f);

		// Primarily for laptops, but can use other input devices.
		if(Input.GetKey(KeyCode.LeftAlt)||Input.GetKey(KeyCode.RightAlt)){
			Rotate(Input.GetAxis ("Horizontal"),Input.GetAxis ("Vertical"));
		}
		if(Input.GetKey(KeyCode.RightShift)){
			Translate(Input.GetAxis ("Vertical"));
		}

	}

	void Rotate (float x, float y){
		Vector3 P = target.position;
		target.RotateAround (P, target.right, -y);
		target.RotateAround (P, target.up, x);
	}

	void Translate (float y){
		target.position+=Vector3.up * y;
	}

	float Cycle(float x){
		if (x > 360) x = x - 360;
		if (x < 0)   x = 360 - x;
		return x;
	}

}
