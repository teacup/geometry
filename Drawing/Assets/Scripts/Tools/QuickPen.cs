﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Quick pen works like pen but instead of assigning a plane, then drawing,
 * the plane is immediately set upon pressing, and resets upon releasing.
 **/
public class QuickPen : AbstractPen {

	[Header("Gizmos")]
	public GameObject ballpoint;
	public Gadget drawingPlane;

	bool atInfinity = false;
	VectorObj root=null;

	// Life Cycle ------------------------------------------------------

	void Start(){
		drawingPlane.renderable = false;
		Colliders.Disable (drawingPlane);
	}

	void Update () {
		ProcessKeyEvents ();
		ProcessMouseEvents ();
		if (!rooted)FloatingUpdate ();
		ballpoint.transform.localScale = Vector3.one * width;
	}

	void OnEnable(){
		drawingPlane.renderable = false;
		Colliders.Disable (drawingPlane);
		drawingPlane.transform.localScale = Vector3.one * 100;
	}

	// Event handling ---------------------------------------------------

	void ProcessKeyEvents(){
		if(Key(MAKE_STROKE_THICKER))  width*=2f;
		if(Key(MAKE_STROKE_THINNER))  width /= 2f;
	}

	void ProcessMouseEvents(){
		if (Input.GetMouseButtonDown (0)) {
			RootPencil ();
			StrokeBuilder.Start (this, atInfinity, null, billboard:true);
		}
		if (Input.GetMouseButtonUp (0)) {
			Destroy (GetComponent<StrokeBuilder> ());
			UnrootPencil ();
		}
	}

	bool Key(KeyCode key){ return Input.GetKeyDown (key); }

	// ------------------------------------------------------------------

	void UnrootPencil(){
		root=null;
		Colliders.Disable (drawingPlane);
		return;
	}

	void RootPencil(){
		root = new VectorObj ();
		root.position = transform.position;
		if (!atInfinity) Colliders.Enable (drawingPlane);
	}

	void FloatingUpdate(){
		RaycastHit hit;
		bool didHit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 100);
		if (didHit) {
			transform.position = hit.point;
			atInfinity = false;
			drawingPlane.renderable=false;
			drawingPlane.transform.position = transform.position;
			drawingPlane.transform.forward = -Camera.main.transform.forward;
		} else {
			drawingPlane.renderable=false;
			transform.position = infinityPosition;
			atInfinity = true;
		}
	}

	// Private properties ---------------------------------------------------------

	Vector3 infinityPosition{
		get{
			var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			return ray.origin + ray.direction * infinityDistance;
		}
	}

	bool rooted{ get{return root != null; } }

	// Nested classes ==============================================================

	class VectorObj{ public Vector3 position; }

}
