﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAction : AbstractAction {

	GameObject target;

	public BasicAction(GameObject target){ this.target = target; }
	override public void Undo(){ target.SetActive (false); }
	override public void Redo(){ target.SetActive (true);  }

}
