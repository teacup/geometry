﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractPen : MonoBehaviour {

	[Header("Parameters")]
	public float infinityDistance = 150f;
	public float decalOffset=0.001f;
	public float width = 1f;
	public Material material;
	[Header("Keys")]
	public KeyCode MAKE_STROKE_THICKER = KeyCode.RightBracket;
	public KeyCode MAKE_STROKE_THINNER = KeyCode.LeftBracket;

}
