﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolSelector : MonoBehaviour {

	KeyCode drawKey     = KeyCode.P;
	KeyCode quickPenKey = KeyCode.Q;
	KeyCode decalKey = KeyCode.O;
	KeyCode deleteKey = KeyCode.E;
	MonoBehaviour[] tools;
	Pen pen;
	QuickPen quickPen;
	DecalPen decalPen;
	Eraser eraser;
	//
	public Text text;

	void Start(){
		pen      = FindObjectOfType<Pen> ();
		eraser   = FindObjectOfType<Eraser> ();
		quickPen = FindObjectOfType<QuickPen> ();
		decalPen = FindObjectOfType<DecalPen> ();
		tools=new MonoBehaviour[]{pen, quickPen, decalPen, eraser};
		Select (quickPen);
	}

	void Update () {
		if (Input.GetKeyDown (drawKey))     Select (pen);
		if (Input.GetKeyDown (decalKey))    Select (decalPen);
		if (Input.GetKeyDown (quickPenKey)) Select (quickPen);
		if (Input.GetKeyDown (deleteKey))   Select (eraser);
		if (Input.GetKeyDown (decalKey))    Select (decalPen);
	}

	void Select(MonoBehaviour tool){ 
		foreach (var t in tools)t.enabled = false; 
		tool.enabled = true;
		text.text = tool.GetType ().Name;
	}

}
