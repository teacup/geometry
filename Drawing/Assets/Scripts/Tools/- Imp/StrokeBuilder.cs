﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrokeBuilder : MonoBehaviour {

	public static int strokeIndex = 0;
	// 
	GameObject surface;
	GameObject stroke;
	bool spherical;
	AbstractPen pen;

	Vector3 anchor;
	List<Vector3> vertices = new List<Vector3>();
	List<Vector3> normals  = new List<Vector3>();

	public float minimumDistance = 3;
	[Header("Mouse")]
	public Vector3 mouse;
	[Header("World")]
	public Vector3 position;
	[Header("Dots")]
	public bool dots = false;
	public float dotScaling = 2f;
	[Header("Ribbon")]
	public bool ribbon = true;
	[Header("Segments")]
	public bool segments = false;
	public bool billboard = false;

	public static void Start(AbstractPen caller, bool spherical, GameObject surface, bool billboard=false){
		var builder=caller.gameObject.AddComponent<StrokeBuilder> ();
		builder.billboard = billboard;
		builder.anchor = caller.transform.position; 
		builder.spherical = spherical;
		if(!spherical)builder.surface = surface;
		builder.pen = caller;
	}

	void Start(){ 
		vertices.Add (new Vector3 ());
		// TODO: This normal should be put in the object's local space,
		// but right here the billboard is not setup yet. Experiment so this can be fixed.
		normals.Add(sampleNormal);
		mouse = Input.mousePosition;
		position = sampleVertex;
		stroke = ribbon ? GameObject.CreatePrimitive(PrimitiveType.Cube): new GameObject("Stroke");
		stroke.transform.SetParent(GameObject.Find("Strokes").transform);
		stroke.tag = "Data";
		if (billboard) stroke.AddComponent<Billboard> ();
		stroke.GetComponent<Renderer> ().material = pen.material;
		Destroy(stroke.GetComponent<Collider>());
		stroke.name = "Stroke "+(strokeIndex++);
		if (ribbon) {
			stroke.GetComponent<Renderer> ().enabled = false;
			stroke.AddComponent<Ribbon> ();
		}
		stroke.transform.position = sampleVertex;
		if(dots) GenerateDot();
	}

	void Update () {
		if (Vector3.Distance (mouse, Input.mousePosition) < minimumDistance) {
			return;
		} else {
			Vector3 P = sampleVertex;
			Vector3 Q = stroke.transform.InverseTransformPoint (P);
			vertices.Add (Q);
			normals.Add (stroke.transform.InverseTransformDirection(sampleNormal));
			if(segments) GenerateSegment ();
			if(dots)     GenerateDot ();
			if (ribbon) GenerateRibbon ();
			mouse = Input.mousePosition;
			position = sampleVertex;
			if (ribbon) stroke.GetComponent<Renderer> ().enabled = true;
		}
			
	}

	void OnDestroy(){
		stroke.AddComponent<MeshCollider> ();
		FindObjectOfType<History>().Append(new BasicAction (stroke));
	}

	void GenerateRibbon(){
		stroke.GetComponent<Ribbon> ().UpdateVertsAndNormals (
			vertices.ToArray(), normals.ToArray(), pen.width, doubleSided:true, flipFaces:true);
	}

	void GenerateSegment(){
		var s = sampleVertex;
		var length = Vector3.Distance (position, s);
		var dir = (s - position).normalized;
		var dot = GameObject.CreatePrimitive (PrimitiveType.Cylinder);
		dot.transform.position = position+dir*length*0.5f;
		dot.transform.localScale = new Vector3(pen.width, length*0.5f, pen.width);
		dot.transform.up = dir;
		dot.transform.SetParent (stroke.transform, worldPositionStays:true);
	}

	void GenerateDot(){
		var dot = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		dot.transform.position = sampleVertex;
		dot.transform.localScale = dotScaling*pen.width*Vector3.one;
		dot.transform.SetParent (stroke.transform, worldPositionStays:true);
	}

	Vector3 sampleVertex{
		get{ 
			var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (spherical) {
				return Camera.main.transform.position+ray.direction * drawingDistance;
			} else {
				RaycastHit hit;
				// TODO: the stroke should be interrupted when we don't hit a surface,
				// and resume when we hit a surface again.
				bool didHit = Physics.Raycast (ray, out hit);
				return hit.point;
			}
		}
	}

	Vector3 sampleNormal{
		get{ 
			var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (spherical) {
				return -ray.direction;
			} else {
				RaycastHit hit;
				// TODO: the stroke should be interrupted when we don't hit a surface,
				// and resume when we hit a surface again.
				bool didHit = Physics.Raycast (ray, out hit);
				return hit.normal;
			}
		}
	}

	float drawingDistance{
		get{ return Vector3.Distance ( Camera.main.transform.position, anchor); }
	}
		
}
