﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractAction {

	public abstract void Undo();
	public abstract void Redo();

}
