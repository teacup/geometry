﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Quick pen works like pen but instead of assigning a plane, then drawing,
 * the plane is immediately set upon pressing, and resets upon releasing.
 **/
public class DecalPen : AbstractPen {

	[Header("Gizmos")]
	public GameObject ballpoint;

	bool atInfinity = false;

	// Life Cycle ------------------------------------------------------

	void Update () {
		ProcessKeyEvents ();
		ProcessMouseEvents ();
		ballpoint.transform.localScale = Vector3.one * width;
	}

	// Event handling ---------------------------------------------------

	void ProcessKeyEvents(){
		if(Key(MAKE_STROKE_THICKER))  width*=2f;
		if(Key(MAKE_STROKE_THINNER))  width /= 2f;
	}

	void ProcessMouseEvents(){
		if (Input.GetMouseButtonDown (0)) {
			StrokeBuilder.Start (this, atInfinity, null);
		}
		if (Input.GetMouseButtonUp (0)) {
			Destroy (GetComponent<StrokeBuilder> ());
		}
	}

	bool Key(KeyCode key){ return Input.GetKeyDown (key); }

	// Private properties ---------------------------------------------------------

	Vector3 infinityPosition{
		get{
			var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			return ray.origin + ray.direction * infinityDistance;
		}
	}

}
