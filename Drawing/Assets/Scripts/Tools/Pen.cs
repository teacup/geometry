﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pen : AbstractPen {

	[Header("Keys")]
	public KeyCode STOP_DRAWING        = KeyCode.Escape;

	[Header("Gizmos")]
	public GameObject ballpoint;
	public Gadget drawingPlane;

	bool atInfinity = false;
	VectorObj root=null;

	// Life Cycle ------------------------------------------------------

	void Start(){
		drawingPlane.transform.localScale = Vector3.one * 1;
	}

	void Update () {
		ProcessKeyEvents ();
		ProcessMouseEvents ();
		if (!rooted)FloatingUpdate ();
		ballpoint.transform.localScale = Vector3.one * width;
	}

	void OnEnable(){
		drawingPlane.renderable = true;
		drawingPlane.transform.localScale = Vector3.one * 1;
	}

	// Event handling ---------------------------------------------------

	void ProcessKeyEvents(){
		if(Key(STOP_DRAWING))         UnrootPencil ();
		if(Key(MAKE_STROKE_THICKER))  width*=2f;
		if(Key(MAKE_STROKE_THINNER))  width /= 2f;
	}

	void ProcessMouseEvents(){
		if (rooted) {
			if (Input.GetMouseButtonUp (1))   UnrootPencil();
			if (Input.GetMouseButtonDown (0)) StrokeBuilder.Start (this, atInfinity, null);
			if (Input.GetMouseButtonUp (0))   Destroy(GetComponent<StrokeBuilder>());
		} else {
			if (Input.GetMouseButton (0)) RootPencil();
		}
	}

	bool Key(KeyCode key){
		return Input.GetKeyDown (key);
	}

	// ------------------------------------------------------------------

	void UnrootPencil(){
		root=null;
		drawingPlane.collider = false;
		return;
	}

	void RootPencil(){
		root = new VectorObj ();
		root.position = transform.position;
		if (!atInfinity) drawingPlane.collider=true;
	}

	void FloatingUpdate(){
		RaycastHit hit;
		bool didHit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 100);
		if (didHit) {
			transform.position = hit.point;
			atInfinity = false;
			drawingPlane.renderable = true;
			drawingPlane.transform.position = transform.position;
			drawingPlane.transform.forward = -Camera.main.transform.forward;
		} else {
			drawingPlane.renderable=false;
			transform.position = infinityPosition;
			atInfinity = true;
		}
	}

	// Private properties ---------------------------------------------------------

	Vector3 infinityPosition{
		get{
			var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			return ray.origin + ray.direction * infinityDistance;
		}
	}

	bool rooted{ get{return root != null; } }

	// Nested classes ==============================================================

	class VectorObj{ public Vector3 position; }

}
