﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThicknessLabel : MonoBehaviour {

	void Update(){
		var pen = FindObjectOfType<Pen> ();
		if (pen) {
			GetComponent<UnityEngine.UI.Text> ().text = FormatMetricDistance (pen.width);
		}
	}

	string FormatMetricDistance(float x){
		if (x < 0.01f) return string.Format ("stroke: {0:0}mm", x * 1000);
		if (x < 1.00f) return string.Format ("stroke: {0:0}cm", x * 100);
		else           return string.Format ("stroke: {0:0}m", x);
	}

}
