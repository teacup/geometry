﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RangeMeter : MonoBehaviour {

	void Update(){
		RaycastHit hit;
		bool didHit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 100);
		GetComponent<UnityEngine.UI.Text>().text = didHit ? FormatMetricDistance(hit.distance):"inf.";
	}

	string FormatMetricDistance(float x){
		if (x < 1f) {
			return string.Format ("range: {0:0.00}cm", x * 100);
		} else {
			return string.Format ("range: {0:0.00}m", x);
		}
	}

}
