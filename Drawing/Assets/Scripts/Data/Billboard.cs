﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

	Transform eye, self;

	void Awake(){ 
		eye  = Camera.main.transform;
		self = transform; 
		Apply (); 
	}

	void Update(){ 
		Apply ();
	}
		
	void Apply () {
		Vector3 P = eye.position;
		P.y = self.position.y;
		transform.LookAt(P);
	}
}
