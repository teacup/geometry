﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Build a mesh representing a ribbon following a curve.
 * TODO: with double sided geometry, normals are wrong. For normals
 * to be correct vertices must be duplicated.
 * TODO: currently this uses 4 vertices per segment. Only 2 verts per segment
 * are needed.
 */
public class Ribbon : MonoBehaviour {

	// forward face ordering
	static int[] ffo = new int[]{ 0, 1, 2, 2, 3, 0 };
	// backward face ordering
	static int[] bfo = new int[]{ 0, 3, 2, 2, 1, 0 };

	const int VERTS_PER_TRI     = 3;
	const int VERTS_PER_SEGMENT = 4;
	const int TRIS_PER_SEGMENT  = 2;

	public bool doubleSided=false;
	public bool flipFaces=false;
	public float nOffset=0.0001f;
	public float width=1f;
	public Vector3[] vertices;
	public Vector3[] normals;

	public void UpdateVerts(List<Vector3> v, float width){
		if (v.Count < 2) return;
		vertices = v.ToArray();
		UpdateMesh(Camera.main.transform.position, width*0.5f);
	}

	public void UpdateVertsAndNormals(Vector3[] vertices, Vector3[] normals, float width, 
		bool doubleSided=false, bool flipFaces=false, float nOffset=0.0001f){
		if (vertices.Length < 2) return;
		this.doubleSided = doubleSided;
		this.flipFaces = flipFaces;
		this.nOffset = nOffset;
		this.width = width;
		this.vertices = vertices;
		this.normals = normals;
		// UpdateMesh(Camera.main.transform.position, width*0.5f);
		UpdateMesh(width*0.5f, nOffset);
	}
		
	void UpdateMesh(float w, float nOffset){
		var n = vertices.Length;
		int sides = doubleSided ? 2 : 1;
		Vector3[] verts   = new Vector3[n*VERTS_PER_SEGMENT];
		Vector3[] norms   = new Vector3[n*VERTS_PER_SEGMENT];
		int[] tris        =  new int[n*TRIS_PER_SEGMENT*VERTS_PER_TRI*sides];
		int V = 0, T = 0;
		for (int i = 0; i < n - 1; i++) {
			//rint ("Normal: " + normals [i]);
			Vector3 Z = normals [i]*nOffset;
			verts [V+0] = PointWithOffsetAndNormal (-w, i  , normals[i])+Z;
			verts [V+1] = PointWithOffsetAndNormal ( w, i  , normals[i])+Z;
			verts [V+2] = PointWithOffsetAndNormal ( w, i+1, normals[i])+Z;
			verts [V+3] = PointWithOffsetAndNormal (-w, i+1, normals[i])+Z;
			norms [V] = norms [V + 1] = norms [V + 2] = norms [V + 3] = normals[i];
			Debug.DrawLine (verts [V + 0], verts [V + 1], Color.blue, 10f);
			Debug.DrawRay (vertices[i], normals[i]*0.1f, Color.blue, 10f);
			// front side
			foreach(var offset in flipFaces?bfo:ffo){
				tris [T++] = V + offset;
			}
			// back side
			if(doubleSided) foreach(var offset in flipFaces?ffo:bfo){
				tris [T++] = V + offset;
			}
			V += VERTS_PER_SEGMENT;
		}
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.normals = norms;
	}

	void UpdateMesh(Vector3 origin, float w){

		var n = vertices.Length;
		Vector3[] verts   = new Vector3[n*VERTS_PER_SEGMENT];
		Vector3[] norms   = new Vector3[n*VERTS_PER_SEGMENT];
		int[] tris        =  new int[n*TRIS_PER_SEGMENT*VERTS_PER_TRI];
		int V = 0, T = 0;
		for (int i = 0; i < n - 1; i++) {
			verts [V+0] = PointWithOffset (-w, at:i  , relativeTo: origin);
			verts [V+1] = PointWithOffset ( w, at:i  , relativeTo: origin);
			verts [V+2] = PointWithOffset ( w, at:i+1, relativeTo: origin);
			verts [V+3] = PointWithOffset (-w, at:i+1, relativeTo: origin);
			norms [V] = norms [V + 1] = norms [V + 2] = norms [V + 3] = Normal (i, origin);
			tris [T+0] = V+2;
			tris [T+1] = V+1;
			tris [T+2] = V+0;
			tris [T+3] = V+0;
			tris [T+4] = V+3;
			tris [T+5] = V+2;
			T += 6;
			V += 4;
		}

		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.normals = norms;

	}

	private Vector3 PointWithOffset(float offset, int at, Vector3 relativeTo){
		return vertices [at] + Tangent (at, relativeTo) * offset;
	}

	private Vector3 PointWithOffsetAndNormal(float offset, int at, Vector3 normal){
		return vertices [at] + TangentWithNormal(at, normal) * offset;
	}

	private Vector3 Tangent(int i, Vector3 origin){
		Vector3[] S = vertices;
		Vector3 u = (i<vertices.Length - 1) ? S[i+1]-S[i] : S[i]-S[i-1];
		Vector3 v = S[i]-origin;
		return Vector3.Cross(u,v).normalized;
	}

	private Vector3 TangentWithNormal(int i, Vector3 normal){
		Vector3[] S = vertices;
		Vector3 u = (i<vertices.Length - 1) ? S[i+1]-S[i] : S[i]-S[i-1];
		return Vector3.Cross(u,normal).normalized;
	}

	private Vector3 Normal(int i, Vector3 origin){
		return (origin - vertices [i]).normalized;
	}
}
