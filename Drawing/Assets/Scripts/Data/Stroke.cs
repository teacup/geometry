﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Stroke {
	
	public Vector3 position;
	public Vector3 euler;
	public float width;
	public bool doubleSided;
	public bool billboard;
	public bool flip;
	public float n;
	public Vector3[] vertices;
	public Vector3[] normals;

	public Stroke(){}

	public Stroke(Ribbon src){
		vertices = src.vertices;
		normals = src.normals;
		width = src.width;
		doubleSided = src.doubleSided;
		flip = src.flipFaces;
		billboard = (src.GetComponent<Billboard> () != null);
		position = src.transform.position;
		euler=src.transform.localEulerAngles;
	}

	public void Load(Ribbon target){
		if (vertices == null) {
			MonoBehaviour.print("No vertices in Data ");
			return;
		}
		if (normals == null) {
			MonoBehaviour.print ("No normals in Data ");
			return;
		}
		target.transform.position = position;
		target.transform.localEulerAngles = euler;
		target.doubleSided = doubleSided;
		if (billboard) target.gameObject.AddComponent<Billboard> ();
		target.UpdateVertsAndNormals(vertices,normals,width,doubleSided,flip,n);
	}

}
