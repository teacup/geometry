﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using System.Xml.Serialization;

public class FileIO : MonoBehaviour {

	const string root="nyan3d";

	enum Mode{LOADING, SAVING, IDLE}
	public GameObject input;
	public InputField field;
	public string filename;
	public string homepath;
	public GameObject toolbox;
	// HACK this is only to get the default material 
	// from the pen. Have to store it because tools become inactive while using IO

	public Pen pen;

	Mode mode = Mode.IDLE;
		
	void Start(){
		pen = FindObjectOfType<Pen> ();
		// referring:
		// https://stackoverflow.com/questions/1143706/getting-the-path-of-the-home-directory-in-c#7404448
		homepath = (Environment.OSVersion.Platform == PlatformID.Unix || 
			Environment.OSVersion.Platform == PlatformID.MacOSX)
			? Environment.GetEnvironmentVariable("HOME")+"/Documents/"+root
			: Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%/"+root);
		try{
			Directory.CreateDirectory(homepath);
		}catch(Exception e){
			print ("Exception occured "+e);
		}
	}

	void Update () {

		if (mode == Mode.IDLE) {
			if (Input.GetKeyUp (KeyCode.PageUp)) {
				print ("open file");
				EnterModalIO ();
				mode = Mode.LOADING;
			}
			if (Input.GetKeyUp (KeyCode.PageDown)) {
				print ("save file");
				EnterModalIO ();
				mode = Mode.SAVING;
			}
		} else {
			if(Input.GetKeyUp(KeyCode.Return)){
				if (field.text.Length <= 0) {
					print ("You need to enter a name");
				} else {
					filename = field.text;
					if (mode == Mode.LOADING) {
						Load ();
					}
					if (mode == Mode.SAVING) {
						Save ();
					}
					ExitModalIO ();
				}
			}
		}
	}

	public void EnterModalIO(){
		input.SetActive (true);
		toolbox.SetActive (false);
		FindObjectOfType<FirstPersonNavigation> ().enabled = false;
	}

	public void ExitModalIO(){
		input.SetActive (false);
		toolbox.SetActive (true);
		FindObjectOfType<FirstPersonNavigation> ().enabled = true;
		mode = Mode.IDLE;
	}

	public void Load(){ 
		print ("Load " + filename); 
		var doc = GameObject.Find ("Strokes");
		DestroyImmediate (doc);
		doc = new GameObject ("Strokes");
		List<Stroke> list = new List<Stroke> ();
		XmlSerializer serializer = new XmlSerializer (list.GetType());
		string path = homepath + "/" + filename;
		StreamReader reader = new StreamReader (path);
		var obj = serializer.Deserialize (reader);
		reader.Close ();
		List<Stroke> strokes=obj as List<Stroke>;
		print ("strokes :" + strokes[0]);
		foreach(Stroke s in strokes){
			var k = GameObject.CreatePrimitive (PrimitiveType.Cube);
			k.GetComponent<Renderer> ().material = pen.material;
			k.name = "Stroke";
			var rib=k.AddComponent<Ribbon> ();
			s.Load (rib);
		}
	}

	/* Referring: http://codesamplez.com/programming/serialize-deserialize-c-sharp-objects*/
	public void Save(){

		print ("Save " + filename); 
		var doc = GameObject.Find ("Strokes");
		List<Stroke> strokes = new List<Stroke> ();
		foreach (Transform child in doc.transform) {
			print ("convert one stroke");
			Ribbon r = child.GetComponent<Ribbon> ();
			Stroke s = new Stroke (r);
			strokes.Add (s);
		}
		string path = homepath + "/" + filename;
		XmlSerializer serializer = new XmlSerializer (strokes.GetType());
		StreamWriter writer = new StreamWriter (path);
		serializer.Serialize (writer, strokes);		
		writer.Close ();
	}

}
