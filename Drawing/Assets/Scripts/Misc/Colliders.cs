﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colliders {

	public static void Enable (Component target) {
		foreach (Collider c in target.GetComponentsInChildren<Collider>()) {
			c.enabled = true;
		}
	}

	public static void Disable (Component target) {
		foreach (Collider c in target.GetComponentsInChildren<Collider>()) {
			c.enabled = false;
		}
	}

	public static void Enable (GameObject target) {
		foreach (Collider c in target.GetComponentsInChildren<Collider>()) {
			c.enabled = true;
		}
	}

	public static void Disable (GameObject target) {
		foreach (Collider c in target.GetComponentsInChildren<Collider>()) {
			c.enabled = false;
		}
	}
}
