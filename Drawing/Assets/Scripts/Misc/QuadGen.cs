﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class QuadGen : MonoBehaviour {
	
	Vector3[] verts = {
		new Vector3 (-0.5f, 0f, 0f),
		new Vector3 (0.5f, 0f, 0f),
		new Vector3 (0.5f, 1f, 0f),
		new Vector3 (-0.5f, 1f, 0f)
	};

	int[] tris = { 
		0, 1, 2, 2, 1, 0, 
		2, 3, 0, 0, 3, 2 
	};

	Vector3[] normals = {
		new Vector3 (0f, 0f, 1f),
		new Vector3 (0f, 0f, 1f),
		new Vector3 (0f, 0f, 1f),
		new Vector3 (0f, 0f, 1f)
	};
		
	void Start() {
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.normals = normals;
	}

}