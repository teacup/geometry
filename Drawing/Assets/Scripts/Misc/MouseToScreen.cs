﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseToScreen : MonoBehaviour {

	public Vector3 screenToWorldPoint;
	public Vector3 screenToRay;
	public float rayLength;

	void Update () {
		var p = Input.mousePosition;	
		screenToWorldPoint=Camera.main.ScreenToWorldPoint (p);
		var ray = Camera.main.ScreenPointToRay (p);
		screenToRay = ray.origin + ray.direction;
		rayLength = ray.direction.magnitude;
		transform.position = screenToRay;
	}
}
