﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonNavigation : MonoBehaviour {

	public float motionSpeed=0.25f;
	float rotation;

	void Update () {
		// 
		if(Input.GetKey(KeyCode.LeftAlt)) return;
		if(Input.GetKey(KeyCode.RightAlt)) return;
		if(Input.GetKey(KeyCode.RightShift)) return;
		bool strafe = Input.GetKey (KeyCode.LeftShift);
		float h = Input.GetAxis ("Horizontal");
		if (!strafe) {rotation += h;}
		transform.localEulerAngles = new Vector3 (0f, rotation, 0f);
		float v = Input.GetAxis ("Vertical");
		transform.position += motionSpeed*(transform.forward * v + transform.right*(strafe?h:0)) ;
	}
}
