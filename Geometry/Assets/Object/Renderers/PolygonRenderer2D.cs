﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PolygonRenderer2D : MonoBehaviour {

	public float triangleSize = 0.95f;

	void Update () {
		var obj = GetComponent<PolygonObject2D> ();
		var polygon = obj.GetShape () as Polygon2D;
		List<Polygon2D> triangles = Triangulator.Triangulate (polygon);
		foreach (var t in triangles) t.Scale (triangleSize);
		MeshBuilder builder = new MeshBuilder ();
		// Passing the transform here is going to inverse-transform vertices.
		// This is needed because PolygonObject2D uses world coordinates, but the
		// mesh needs vertices in local coordinates.
		builder.AddAllTriangles(triangles, transform);
		meshFilter.mesh = builder.mesh;
		if (!GetComponent<MeshRenderer> ()) gameObject.AddComponent<MeshRenderer> ();
	}

	MeshFilter meshFilter{
		get{
			var filter=GetComponent<MeshFilter> ();
			if (filter == null)filter = gameObject.AddComponent<MeshFilter> ();
			return filter;
		}
	}

}
