﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PolygonProjectionOp : MonoBehaviour {

	public PolygonObject3D target;
	Polygon2D projection;

	public void Update(){

		if (target == null) return;
		Polygon3D poly3 = target.GetShape () as Polygon3D;
		projection = poly3.polygon2D;
		//ebug.Log (projection);
		PolygonObject2D.Draw (projection,Color.yellow);

	}

}
