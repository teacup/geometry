﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Intersection2D : MonoBehaviour {

	public GeometryObject2D A;
	public GeometryObject2D B;
	public Vector3 intersection;

	void Update () {

		if (A == null)
			return;
		if (B == null)
			return;

		try{
			Shape2D result = A.GetShape().Intersect(B.GetShape());
			if(result as Point2D!=null){
				Vector2 p2 = (result as Point2D).val;
				Vector3 p3 = new Vector3(p2.x, 0, p2.y);
				transform.position = p3;
			}
			GetComponent<MeshRenderer>().enabled=true;
		}catch(System.Exception){
			//ebug.Log ("an exception occured: "+ex);
			GetComponent<MeshRenderer>().enabled=false;
		}
	}

}
