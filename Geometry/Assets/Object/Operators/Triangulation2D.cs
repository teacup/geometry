﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Triangulation2D : MonoBehaviour {

	public PolygonObject2D target;

	public void Update(){

		if (target == null) return;
		Polygon2D polygon = target.GetShape () as Polygon2D;
		List<Polygon2D> tris = Triangulator.Triangulate (polygon);

		foreach (Polygon2D p in tris) {
			DrawTriangle (p.corners);
			Polygon2D smaller = p.Duplicate ();
			smaller.Scale (0.9f);
			DrawTriangle (smaller.corners);
		}

	}

	private void DrawTriangle(List<Vector2> corners){

		List<Vector3> l = new List<Vector3> ();
		foreach (Vector2 p in corners) {
			l.Add (new Vector3 (p.x, 0, p.y));
		}
		for (int i = 0; i < corners.Count; i++) {
			Debug.DrawLine (l [i], l [(i + 1) % 3]);
		}

	}

}
