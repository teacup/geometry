﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class GroundxMeshOp : MonoBehaviour {

	public MeshObject3D mesh;
	public List<Polygon3D> cuts;

	void Update(){

		if (mesh == null)return;
		Mesh3D m = mesh.GetShape () as Mesh3D;
		cuts = GroundPlane.Cut (m);

	}

	void OnDrawGizmos(){

		if (cuts != null) {
			foreach(Polygon3D e in cuts){
				PolygonObject3D.Draw (e*0.8f);
			}
		}

	}

}
