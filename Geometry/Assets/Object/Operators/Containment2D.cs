﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Containment2D : MonoBehaviour {

	public GeometryObject2D container;
	public GeometryObject2D candidate;

	public void OnDrawGizmos(){

		if (container == null) return;
		if(candidate==null) return;
		
		if (container.GetShape().Contains (candidate.GetShape())) {
			Gizmos.DrawWireSphere (candidate.transform.position, 0.5f);
		}

	}

}
