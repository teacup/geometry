﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class GroundxPolygonOp : MonoBehaviour {

	public PolygonObject3D polygon;
	private List<Point3D> points;
	public List<Polygon3D> cuts;

	void Update(){

		Polygon3D p = polygon.GetShape () as Polygon3D;
		points = GroundPlane.GetIntersectionPoints(p);
		cuts = GroundPlane.Cut (p);

	}

	void OnDrawGizmos(){

		foreach (Point3D point in points) {
			Vector3 P = GeometryObject3D.ToVector3 (point);
			Gizmos.DrawWireSphere (P, 0.2f);
		}
		if (cuts != null) {
			foreach(Polygon3D e in cuts){
				PolygonObject3D.Draw (e*0.8f);
			}
		}

	}

}
