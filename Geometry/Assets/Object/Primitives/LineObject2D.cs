﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LineObject2D : GeometryObject2D {

	override public Shape2D GetShape(){
		Transform T = transform;
		return new Line2D (T.position, T.forward, ProjectionRule.X_Z);
	}

	void Update () {
		Transform T = transform;
		Debug.DrawRay(T.position,T.forward*10f);
	}

}
