﻿using UnityEngine;
using System.Collections;

public class MeshObject3D : GeometryObject3D {

	override public Shape3D GetShape(){

		Mesh3D mesh = new Mesh3D ();
		foreach (PolygonObject3D P in GetComponentsInChildren<PolygonObject3D>()) {
			mesh+= (P.GetShape () as Polygon3D);
		} return mesh;

	}

}
