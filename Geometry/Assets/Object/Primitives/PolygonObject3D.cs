﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class PolygonObject3D : GeometryObject3D {

	public Transform[] corners;

	override public Shape3D GetShape(){

		List<Point3D> list = new List<Point3D> ();
		if (IsImplicit) {
			foreach (Transform t in transform) {
				Point3D point = ToPoint3D (t.position);
				list.Add (point);
			}
		} else {
			foreach (Transform t in corners) {
				Point3D point = ToPoint3D (t.position);
				list.Add (point);
			}
		} return new Polygon3D (list);

	}

	public bool IsImplicit{
		get{ 
			if(corners==null)return true; 
			if(corners.Length==0)return true;
			return false;
		}
	}

	public bool IsExplicit{ get{ return !IsImplicit; } }

	// life-cycle -----------------------------------

	void Update(){
		Polygon3D polygon = GetShape () as Polygon3D;
		if (!IsImplicit) {
			transform.position = ToVector3 (polygon.center);
			transform.forward = ToVector3 (polygon.normal);
		}
		Draw (polygon);
	}

	// utilities ------------------------------------

	public static void Draw(Polygon3D polygon){
		Vector3 C = ToVector3 (polygon.center);
		Vector3 n = ToVector3 (polygon.normal);
		Debug.DrawRay (C, n*0.1f,Color.yellow);
		foreach (Segment3D s in polygon.segments) {
			Debug.DrawLine (
				ToVector3 (s.A),
				ToVector3 (s.B)
			);
		}
		polygon *= 0.95f;
		foreach (Segment3D s in polygon.segments) {
			Debug.DrawLine (
				ToVector3 (s.A),
				ToVector3 (s.B)
			);
		}
	}

}