﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SegmentObject2D : GeometryObject2D {

	public Transform A;
	public Transform B;

	override public Shape2D GetShape(){
		return new Segment2D (A.position, B.position, ProjectionRule.X_Z);	
	}

	public void Update(){

		if (A && B) {
			Debug.DrawLine (A.position, B.position);
		}
	}

}
