﻿using UnityEngine;
using System.Collections;

public class PointObject2D : GeometryObject2D {

	override public	Shape2D GetShape(){
		Vector2 pos = Projection.project (transform.position, ProjectionRule.X_Z);
		return new Point2D (pos);
	}

}
