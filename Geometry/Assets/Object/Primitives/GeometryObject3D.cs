﻿using UnityEngine;
using System.Collections;

public abstract class GeometryObject3D : MonoBehaviour {

	public abstract Shape3D GetShape();

	public static Point3D ToPoint3D(Vector3 P){
		return new Point3D (P.x, P.y, P.z);
	}

	public static Vector3D ToVector3D(Vector3 P){
		return new Vector3D (P.x, P.y, P.z);
	}

	public static Vector3 ToVector3(Vector3D P){
		return new Vector3 (P.x, P.y, P.z);
	}
	
	public static Vector3 ToVector3(Point3D P){
		return new Vector3 (P.x, P.y, P.z);
	}

}
