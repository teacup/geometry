﻿using UnityEngine;
using System.Collections;

public abstract class GeometryObject2D : MonoBehaviour {

	public abstract Shape2D GetShape();

}