﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class PolygonObject2D : GeometryObject2D {

	override public Shape2D GetShape(){

		List<Vector2> corners = new List<Vector2> ();
		foreach(Transform t in transform){
			Vector2 point = Projection.project (t.position, ProjectionRule.X_Z);
			corners.Add (point);
		}
		return new Polygon2D (corners);

	}

	// life-cycle 

	void Update(){

		int c = transform.childCount;
		for(int i=0;i<c;i++){
			Vector3 A = transform.GetChild (i).position;
			Vector3 B = transform.GetChild ((i+1)%c).position;
			Debug.DrawLine (A, B);
		}

	}

	public static void Draw(Polygon2D p){
		Draw (p, Color.white);
	}

	public static void Draw(Polygon2D p, Color color){
		foreach (Segment2D s in p.segments) {
			Vector3 A = new Vector3(s.A.x, s.A.y, 0f);
			Vector3 B = new Vector3(s.B.x, s.B.y, 0f);
			Debug.DrawLine (A, B, color);
		}
	}

}
