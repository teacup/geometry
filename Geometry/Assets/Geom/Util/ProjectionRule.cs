﻿
public enum ProjectionRule {

	X_Y, Y_X,
	Y_Z, Z_Y,
	Z_X, X_Z

}
