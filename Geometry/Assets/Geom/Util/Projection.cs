﻿using UnityEngine;
using System.Collections;

public class Projection {

	public static Vector2 project(Vector3 p,ProjectionRule pp){
		switch (pp) {
		case ProjectionRule.X_Y:
			return new Vector2 (p.x,p.y);
		case ProjectionRule.Y_Z:
			return new Vector2 (p.y,p.z);
		case ProjectionRule.Z_X:
			return new Vector2 (p.z,p.x);
		case ProjectionRule.Y_X:
			return new Vector2 (p.y,p.x);
		case ProjectionRule.Z_Y:
			return new Vector2 (p.z,p.y);
		case ProjectionRule.X_Z:
			return new Vector2 (p.x,p.z);
		default:
			throw new System.Exception ("Unkown projection rule");
		}
	}

}
