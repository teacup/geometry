﻿using UnityEngine;
using System.Collections.Generic;

/**
 * This triangulator implements the 'ear trimming' strategy.
 * Non convex polygons are supported; self intersecting polygons are not.
 * Note: might be improved referring to: https://en.wikipedia.org/wiki/Polygon_triangulation
 */
public class Triangulator : MonoBehaviour {

	/**
	 * The triangulator handles both convex and non-convex polygons.
	 * If 'pretty' is true the output will have a more regular pattern, otherwise
	 * many triangles end up connected to the same vertex.
	 * TODO: there are a few edge cases where this cannot triangulate completely (error),
	 * would be nice to hunt them down.
	 * TODO: in some cases, incorrect triangulation is observed. The known case is when
	 * a ray for insideness is going through corners. 
	 */
	public static List<Polygon2D> Triangulate(Polygon2D input, bool pretty=true){
		List<Polygon2D> list = new List<Polygon2D> ();
		while (input.count >= 3) {
			bool added = false;
			for(int i=0;i<input.count;i++){
				int c = input.count;
				Polygon2D tri = Create (input, i, (i+1)%c, (i+2)%c);
				if (tri != null) {
					list.Add (tri);
					added = true;
					if(!pretty) break;
				}
			}
			if (!added) {
				if (input.count < 3) {
					Debug.Log ("Note: finished with " + input.count);
					return list;
				}
				Debug.LogError ("cannot triangulate ("+input.count+" corners left)");
				break;
			}
		}
		return list;
	}
		
	/**
     * This function generates a candidate triangle from (presumably) consecutive
     * corners A, B, C; A-B, B-C are on the edges of the polygon.
     * Then we want to find out whether that candidate is lying within the polygon, or outside,
     * as would be the case if the corner is concave.
     * To test whether the candidate triangle is inside the polygon, we check whether A-C is inside.
     * Note: experimentally verified that, if we checked the center of the triangle instead,
     * sometimes the center would be inside, but the edge A-C would not.
     * If a valid candidate is found, B is removed from the input.
	 */
	private static Polygon2D Create(Polygon2D input, int a, int b, int c){
		Vector3 A = input.Get (a),  B = input.Get (b), C = input.Get (c);
		Polygon2D tri = new Polygon2D (A, B, C);
		Vector2 M = (A+C)*0.5f;
		if (input.count == 3 || input.ContainsPoint (M)) {
			// if the trim (AC) intersects with non connected edges from the polygon, 
			// it is invalid.
			if (TrimIntersects (input, b)) return null;
			DebugDrawPoint (M);
			input.Remove (b);
			return tri;
		} else return null;
	}

	/**
	 * Assuming that we trim corner i from the input, check whether 
	 * the trimming edge would intersect the input.
	 * For this purpose, we discard edges from (i-2) to (i+2), which are
	 * connected to the trim; so we're only going to test against other edges.
	 */
	static bool TrimIntersects(Polygon2D input, int i){
		// Since edges from -2 to +2 are discarded, the minimum number of corners
		// for a self-intersection to occur is 5.
		if (input.count < 5) return false;
		List<Segment2D> edges = input.Edges (i+2, i-2);
		Segment2D trim = new Segment2D (input.Get (i - 1, cycle:true), input.Get (i + 1, cycle: true));
		foreach (var e in edges) 
			if (e.Intersects(trim)) 
				return true;
		return false;
	}

	static void DebugDrawPoint(Vector2 P) {
		Vector3 A = new Vector3(P.x, 0f, P.y);
		Vector3 B = new Vector3(P.x+0.05f, 0f, P.y+0.05f);
		Debug.DrawLine (A, B, Color.green);
	}
}
