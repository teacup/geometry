﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Polygon2D : Shape2D {

	public List<Vector2> corners;

	// Constructor ------------------------------------------------------------------

	public Polygon2D(params Vector2[] corners){

		this.corners = new List<Vector2>(corners);

	}

	public Polygon2D(List<Vector2> corners){

		this.corners = corners;

	}

	// Operations ------------------------------------------------------------------

	public Polygon2D Duplicate(){

		List<Vector2> lcopy = new List<Vector2> ();
		foreach (Vector2 p in corners) {
			lcopy.Add (new Vector2 (p.x, p.y));
		}
		return new Polygon2D (lcopy); 
	}

	public Vector2 Get(int i, bool cycle=false){ 
		if (cycle && count>0) {
			while (i < 0) i += count;
			i = i % count;
		}
		return corners [i]; 
	}

	public Vector2 Remove(int i){

		Vector2 removed = corners [i];
		corners.RemoveAt (i);
		return removed;

	}

	public void Scale(float factor){

		Vector2 O = center;
		for (int i = 0; i < count; i++) {
			Vector2 u = corners [i]-center;
			corners [i] = O + (u * factor);
		}

	}

	// -----------------------------------------------------------------------------

	override public bool Contains (Shape2D shape){

		if (shape.GetType () == typeof(Point2D)) {
			return ContainsPoint ((shape as Point2D).val);
		}
		throw new System.Exception ("["+shape+" inside"+ this +"]: unimplemented");

	}

	public bool ContainsPoint(Vector2 p){
		int inside = 0;
		int outside = 0;
		if (ContainsPointImp (p, Vector3.right)) inside++; else outside++;
		if (ContainsPointImp (p, Vector3.left )) inside++; else outside++;
		if (ContainsPointImp (p, Vector3.up   )) inside++; else outside++;
		if (ContainsPointImp (p, Vector3.down )) inside++; else outside++;
		return inside > outside ? true : false;
	}

	/**
	 * This containment test works by casting a ray from 'p', and checking
	 * how many times the ray intersects the shape. If the number of intersections
	 * is odd, we are inside.
	 * This method is liable to numerical approximations.
	 */
	public bool ContainsPointImp(Vector2 p, Vector3 dir){

		Ray2D ray = new Ray2D (p, dir);

		int c = corners.Count, intersections=0;

		for (int i = 0; i < c; i++) {
			Vector2 A = corners [i];
			Vector2 B = corners [(i + 1)%c];
			Segment2D edge = new Segment2D (A, B);
			try{
				edge.Intersect(ray);
				intersections++;
			}catch(System.Exception){}
		}
		return (intersections % 2)==1;

	}

	public bool IsCorner(Vector2 p){
		foreach (Vector2 k in corners) {
			if (p == k) return true;
		}return false;
	}

	override public Shape2D Intersect(Shape2D shape){
		throw new System.Exception ("Unimplemented");
	}

	// PROPERTIES ------------------------------------------------------------------

	public float radius {
		get{
			float R = 0f;
			for (int i = 0; i < corners.Count; i++) {
				for (int j = i + 1; j < corners.Count; j++) {
					float r = (corners [i] - corners [j]).magnitude;
					if (r > R) R = r;
				}
			} return R;
		}
	}

	public Vector2 center{
		get{
			Vector2 result=Vector2.zero;
			foreach (Vector2 p in corners) {
				result += p;
			}
			result /= corners.Count;
			return result;
		}
	}

	public int count{get{return corners.Count;}}

	public List<Segment2D> Edges(int start, int end){
		List<Segment2D> list = new List<Segment2D>();
		if (count > 0) {
			while (start < 0) start += count;
			while (end < 0) end += count;
			start = start % count;
			end = end % count;
		}
		int i = start;
		//ebug.Log ("Accumulate edges from " + start + " to " + end);
		do {
			int j = (i+1)%count;
			list.Add(new Segment2D(corners[i], corners[j]));
			i=j;
		}while(i!=end);
		return list;
	}

	public List<Segment2D> segments{
		get{
			List<Segment2D> list = new List<Segment2D>();
			for(int i=0;i<count;i++){
				Segment2D s = new Segment2D(corners[i], corners[(i+1)%count]);
				list.Add (s);
			} return list;
		}
	}

	// operations -------------------------------------------------------------

	public static Polygon2D operator +(Polygon2D polygon, Point2D point) {
		polygon.corners.Add (new Vector2(point.x,point.y));
		return polygon;
	}

	override public string ToString(){
		string s = "";
		foreach(Vector2 p in corners)s+=p+" ";
		return "Polygon2D: "+count+"{ "+s+" }";
	}

}
