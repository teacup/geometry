﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Union2D : Shape2D {

	public List<Shape2D> children;

	public Union2D (List<Shape2D> children){
		this.children = children;
	}

	override public Shape2D Intersect(Shape2D shape){
		throw new System.Exception ("Unimplemented");
	}

	override public bool Contains (Shape2D shape){
		throw new System.Exception ("Unimplemented");
	}
}
