﻿using UnityEngine;
using System.Collections;

public class Point2D : Shape2D {

	private Vector2 point;

	public Point2D(float x,float y){
		point = new Vector2 (x, y);
	}

	public Point2D(Vector2 p){
		point = p;
	}

	// Shape2D ------------------------------------------

	override public bool Contains (Shape2D shape){
		throw new System.Exception ("unimplemented");
	}

	override public Shape2D Intersect(Shape2D shape){
		throw new System.Exception ("Unimplemented");
	}

	// properties ----------------------------------------

	public float x{
		get{return point.x;}
	}

	public float y{
		get{return point.y;}
	}

	public Vector2 val{ get{ return point;} }

}
