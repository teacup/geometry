﻿using UnityEngine;
using System;

public class Line2D : Shape2D {

	private Cartesian c;
	private Parametric p;

	// Constructors ----------------------------------------------------

	public Line2D(Vector2 origin, Vector2 direction){
		p = new Parametric (origin, direction);
	}

	public Line2D(Vector3 origin, Vector3 direction, ProjectionRule r){
		p = new Parametric (
			Projection.project(origin,r), 
			Projection.project(direction,r));
	}

	public Line2D(float a, float b, float c){
		this.c = new Cartesian (a, b, c);
	}

	// <Shape2D> --------------------------------------------------------

	override public bool Contains (Shape2D shape){
		throw new System.Exception ("unimplemented");
	}

	override public Shape2D Intersect(Shape2D that){

		Type thisType = this.GetType();
		Type thatType = that.GetType();

		if (thatType == typeof(Line2D)) {
			return Line2DxLine2D.Intersect (this, that as Line2D );
		} else if (thatType == typeof(Segment2D)) {
			return Line2DxSegment2D.Intersect (this, that as Segment2D );
		} else {
			throw new System.Exception ("Don't know how to intersect " 
				+ thisType.Name + " with " + thatType.Name);
		}

	}

	// ------------------------------------------------------------------

	public Cartesian cartesian{
		get{ if (c == null) c = new Cartesian (p); return c; }
	}

	public Parametric parametric{
		get{ if (p == null) p = new Parametric (c); return p; }
	}

	// -------------------------------------------------------------------

	public class Cartesian{

		private float A,B,C;

		public Cartesian(float A, float B, float C){ 
			this.A=A; 
			this.B=B; 
			this.C=C; 
		}

		/**
		 * X = Ax + k.Ux
		 * Y = Ay + k.Uy
		 * <=>
		 * k = (X-Ax)/Ux
		 * Y = Ay + Uy*((X-Ax)/Ux)
		 * <=>
		 * Y*Ux = Ay*Ux +Uy*(X-Ax)
		 * <=>
		 * (- Uy)*X + Y*Ux = (Ay*Ux - Uy*Ax)
		 * A = -Uy
		 * B = Ux
		 * C = (Ay*Ux - Uy*Ax)
		 */
		public Cartesian(Parametric p){
			A = - p.direction.y;
			B =   p.direction.x;
			C =   p.origin.y * p.direction.x - p.origin.x * p.direction.y;
			if(c!=0f){ A/=C; B/=C; C/=C; }
		}

		public float a{ get{ return A; }}
		public float b{ get{ return B; }} 
		public float c{ get{ return C; }} 
	}

	// -------------------------------------------------------------------

	public class Parametric{

		private Vector2 o,d;

		public Parametric(Vector2 o, Vector2 d){ 
			this.o=o;
			this.d=d; 
		}

		public Parametric(Cartesian c){
			Debug.LogError("not implemented");
			return;
		}

		public Vector2 origin{get{ return o; }}
		public Vector3 direction{get{ return d;}} 

	}

}