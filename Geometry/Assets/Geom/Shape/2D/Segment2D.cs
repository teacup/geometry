﻿using UnityEngine;
using System;

public class Segment2D : Shape2D {

	private const float THRESHOLD=0.000001f;

	private Vector2 PointA,PointB;

	// Constructors ---------------------------------------------

	public Segment2D(Vector2 Pa,Vector2 Pb){
		
		PointA = Pa;
		PointB = Pb;

	}

	public Segment2D(Vector3 Pa,Vector3 Pb, ProjectionRule r){

		PointA = Projection.project(Pa,r);
		PointB = Projection.project(Pb,r);

	}

	// public ----------------------------------------------------

	override public bool Contains (Shape2D shape){ throw new System.Exception ("unimplemented"); }

	public bool Contains(Point2D point){
		
		Vector2 P = point.val;
		float	a = (P - A).magnitude, 
				b = (P - B).magnitude, 
				val = Mathf.Abs((a + b) - magnitude);

		return Mathf.Abs (val) < THRESHOLD;

	}

	public bool Intersects(Shape2D that){
		try{
			Intersect(that);
			return true;
		}catch{
			return false;
		}
	}

	// <Shape2D> -------------------------------------------------

	override public Shape2D Intersect(Shape2D that){

		Type thisType = this.GetType();
		Type thatType = that.GetType();

		if (thatType == typeof(Line2D)) {
			return Line2DxSegment2D.Intersect (that as Line2D, this );
		} else if (thatType == typeof(Segment2D)) {
			return Segment2DxSegment2D.Intersect (this, that as Segment2D );
		} else if (thatType == typeof(Ray2D)) {
			return Ray2DxSegment2D.Intersect (that as Ray2D, this );
		} else if (thatType == typeof(Polygon2D)){
			return Segment2DxPolygon2D.Intersect(this, that as Polygon2D);
		} else {
			throw new System.Exception ("Don't know how to intersect " 
				+ thisType.Name + " with " + thatType.Name);
		}

	}

	// PROPERTIES -------------------------------------------------

	public Vector2 A{ get{ return PointA; } }

	public Vector2 B{ get{ return PointB; } }

	public float magnitude{ get{ return (A - B).magnitude; } }

	override public string ToString(){
		return "Segment{ " + A + ", " + B + " }";
	}

}
