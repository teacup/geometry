﻿using UnityEngine;
using System.Collections;

public abstract class Shape2D { 

	public abstract bool Contains (Shape2D shape);
	public abstract Shape2D Intersect (Shape2D shape);

}