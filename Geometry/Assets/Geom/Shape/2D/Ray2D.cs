﻿using UnityEngine;
using System;

public class Ray2D : Shape2D {
	
	Vector2 o;
	Vector2 d;

	public Ray2D(Vector2 o,Vector2 d){
		this.o = o;
		this.d = d;
	}

	override public Shape2D Intersect(Shape2D that){

		//Type thisType = this.GetType();
		Type thatType = that.GetType();

		if (thatType == typeof(Segment2D)) {
			return Ray2DxSegment2D.Intersect (this, that as Segment2D);
		} else {
			throw new System.Exception ("Unimplemented");
		}

	}

	override public bool Contains(Shape2D shape){
		if (shape.GetType () == typeof(Point2D)) {
			return this.Contains ((shape as Point2D).val);
		} else {
			throw new System.Exception ("unimplemented");
		}
	}

	public bool Contains(Vector2 p){
		//ebug.Log ("check ray contains shape");
		float alpha = Vector2.Angle (d, o - p);
		//ebug.Log ("alpha: " + alpha);
		return alpha < 0.00001f;

	}

	public Vector2 origin{get{return o;}}
	public Vector2 direction{get{return d;}}

}
