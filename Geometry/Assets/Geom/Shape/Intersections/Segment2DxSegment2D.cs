﻿using UnityEngine;
using System.Collections;

public class Segment2DxSegment2D {

	public static Shape2D Intersect (Segment2D s0, Segment2D s1){

		Line2D l0 = new Line2D (s0.A, s0.B - s0.A);
		Line2D l1 = new Line2D (s1.A, s1.B - s1.A);
		Point2D I = l0.Intersect (l1) as Point2D;
		if (s0.Contains (I) && s1.Contains (I)) {
			return I;
		} else {
			throw new System.Exception ("Intersection not contained in segment");
		}

	}

}
