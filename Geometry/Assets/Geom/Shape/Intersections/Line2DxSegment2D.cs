﻿using UnityEngine;
using System.Collections;

public class Line2DxSegment2D {

	public static Shape2D Intersect (Line2D line, Segment2D segment){

		Line2D implied = new Line2D (segment.A, segment.B-segment.A);
		Point2D P = line.Intersect (implied) as Point2D;
		if (segment.Contains (P)) {
			return P;
		} else {
			throw new System.Exception ("Segment does not contain intersection");
		}

	}

}
