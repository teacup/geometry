﻿using UnityEngine;
using System;

public class Line2DxLine2D {

	// ax + by = c <=> x = (c-by)/a
	// dx + ey = f <=>  x = (f-ey)/d
	public static Shape2D Intersect (Line2D A, Line2D B){
		Line2D.Cartesian l1 = A.cartesian;
		Line2D.Cartesian l2 = B.cartesian;
		float D = (l1.b * l2.a - l2.b * l1.a);
		if (D == 0)	throw new Exception ("Determinant is zero");
		float x = (l2.c * l1.b - l1.c * l2.b) / D;
		float y = (l1.c * l2.a - l2.c * l1.a) / D;
		return new Point2D(new Vector2 (x, y));

	}

}
