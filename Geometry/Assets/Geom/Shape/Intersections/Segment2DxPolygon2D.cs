﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Segment2DxPolygon2D {

	public static Shape2D Intersect (Segment2D s, Polygon2D p){
		var edges = p.segments;
		List<Shape2D> I = new List<Shape2D> ();
		foreach (var e in edges) {
			Shape2D i = e.Intersect (s);
			if (i != null) {
				I.Add (i);
			}
		}
		if (I.Count == 0) {
			throw new System.Exception ("Intersection not contained in segment");
		} else {
			return new Union2D (I);
		}
	}

}
