﻿using UnityEngine;
using System.Collections;

public class Ray2DxSegment2D {

	public static Shape2D Intersect (Ray2D ray, Segment2D segment){

		Line2D l0 = new Line2D (ray.origin, ray.direction);
		Line2D l1 = new Line2D (segment.A, segment.B - segment.A);
		Point2D I = l0.Intersect (l1) as Point2D;

		if (!segment.Contains (I)) {
			throw new System.Exception ("Intersection not in segment");
		} else if (!ray.Contains (I.val)) {
			throw new System.Exception ("Intersection not contained in segment");
		} else {
			return I;
		}

	}

}
