﻿using System.Collections;
using System.Collections.Generic;


public class Polygon3D : Shape3D, IEnumerable {

	private List<Point3D> corners;

	public Polygon3D(){ corners = new List<Point3D>(); }
	public Polygon3D(List<Point3D> points){ corners = points; }

	// operations ------------------------------------

	public void Add(Point3D corner){ 
		corners.Add (corner); 
	}

	public void Scale(float factor){
		Point3D C = center;
		for(int i=0;i<count;i++) {
			corners[i] = C + (corners[i] - C) * factor;
		}
	}

	public Polygon3D Duplicate(){
		List<Point3D> corners = new List<Point3D> ();
		foreach (Point3D P in this.corners) {
			corners.Add (P.Duplicate ());
		}
		return new Polygon3D (corners);
	}

	// properties ----------------------------------------------------------
	
	public Point3D center{ get{  return Point3D.GetCenter (corners); } }

	public int count { get { return corners.Count; } }
	
	public Segment3D[] segments{
		get{
			Segment3D[] array = new Segment3D[count];
			for (int i = 0; i < count; i++) {
				array [i] = new Segment3D (this [i], this [(i + 1) % count]);
			}
			return array;
		}
	}

	public Vector3D normal{
		get{
			Vector3D W = new Vector3D(0f,0f,0f);
			for(int i=0;i<count;i++){
				Vector3D u = corners[(i+1)%count]-corners[i];
				for(int j=i+1;j<count;j++){
					Vector3D v = corners[(j+1)%count]-corners[j];
					Vector3D w = Vector3D.Cross(u,v);
					W+=w;
				}
			}
			return W.normalized;
		}
	}

	public Polygon2D polygon2D{
		get{
			Polygon2D P = new Polygon2D();
			Quaternion3D R = normal.RotationTo (Vector3D.forward);
			foreach(Point3D p0 in corners){
				Point3D p1 = R*p0;
				Point2D p1_2d = p1.xy;
				P+=p1_2d;
			}
			return P;
		}
	}

	// operators -------------------------------------

	public static Polygon3D operator *(Polygon3D original, float x){
		Polygon3D copy = original.Duplicate();
		copy.Scale (x);
		return copy;
	}
		
	public static Polygon3D operator +(Polygon3D A, Polygon3D B) {
		A.corners.AddRange (B.corners);
		return A;
	}

	public static Polygon3D operator +(Polygon3D polygon, Point3D point) {
		polygon.corners.Add (point);
		return polygon;
	}

	public Point3D this[int i]{ get{ return corners [i]; } }

	public IEnumerator GetEnumerator(){ foreach (Point3D e in corners) yield return e; }

}
