﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GroundPlane : Shape3D{

	public static List<Polygon3D> Cut(Mesh3D mesh){
		List<Polygon3D> output = new List<Polygon3D> ();
		foreach (Polygon3D p in mesh) output.AddRange (Cut (p));
		return output;
	}

	/**
	 * Note: the simple method used here is only expected work when there is only one cut and
	 * the points are mostly coplanar (If you cut the legs of an 'M' shaped polygon that won't work).
	 * Also, this will fail if a point lies exactly at y = 0 so, beware.
	 */
	public static List<Polygon3D> Cut(Polygon3D polygon){

		int c = polygon.count;
		List<Polygon3D> output = new List<Polygon3D> ();
		Polygon3D P = new Polygon3D ();
		output.Add (P);
		for (int i = 0; i < c; i++) {		
			Point3D A = polygon [i], B = polygon [(i+1)%c];
			P+=A; try{
				Point3D X = IntersectSegment(A,B);
				P+=X;
				P = new Polygon3D();
				P+=X;
				output.Add(P);
			}catch(Exception){}
		}
		if (output.Count == 3) {
			output[0]+=output[2];
			output.RemoveAt (2);
		}
		return output;

	}

	public static List<Point3D> GetIntersectionPoints(Polygon3D polygon){

		List<Point3D> output = new List<Point3D> ();
		foreach (Segment3D seg in polygon.segments) {
			try{
				output.Add(Intersect(seg));
			}catch(System.Exception){}
		}
		return output;

	}

	public static Point3D Intersect(Segment3D segment){

		Point3D A = segment.A;
		Point3D B = segment.B;
		return IntersectSegment (A, B);

	}

	/**
	 * Ay + k.Uy = 0
	 * k = - (Ay / Uy)
	 * with: U = (B-A)
 	 */
	private static Point3D IntersectSegment(Point3D A, Point3D B){

		float k = A.y / (A.y - B.y);
		Point3D I = new Point3D (
			A.x + (B.x - A.x) * k,
			0,
			A.z + (B.z - A.z) * k
		);
		if (I.IsAbove (A, B) || I.IsBelow (A, B)) {
			throw new System.Exception ("no intersection");
		} else {
			return I;
		}

	}

}
