﻿using System.Collections;
using System.Collections.Generic;

public class Mesh3D : Shape3D {

	private List<Polygon3D> faces;

	// Constructor ----------------------------------------------------------------------

	public Mesh3D()							{ this.faces = new List<Polygon3D>(); }
	public Mesh3D(List<Polygon3D> faces)	{ this.faces = faces; 			 }

	// Getting information --------------------------------------------------------------

	public Polygon3D this[int i]{ get{ return faces[i]; } }
	public IEnumerator GetEnumerator(){ foreach (Polygon3D e in faces) yield return e; }

	// Operations -----------------------------------------------------------------------

	public static Mesh3D operator +(Mesh3D mesh, Polygon3D polygon) {
		mesh.faces.Add (polygon);
		return mesh;
	}

	public static Mesh3D operator -(Mesh3D mesh, Polygon3D polygon) {
		mesh.faces.Remove (polygon);
		return mesh;
	}

}
