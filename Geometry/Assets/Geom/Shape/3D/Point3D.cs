﻿using System.Collections.Generic;

public class Point3D : Tuple3D, Shape3D {

	public static Point3D origin{ get { return new Point3D (0f, 0f, 0f); } }

	// Constructor ------------------------------------------

	public Point3D (float x, float y, float z) : base(x,y,z) {}

	// Operations -------------------------------------------

	public Point3D Duplicate(){ return new Point3D (x, y, z); }
	
	// Getting information -----------------------------

	public bool IsAbove(params Point3D[] points){
		foreach (Point3D p in points) {
			if (this.y<p.y) return false;
		} return true;
	}

	public bool IsBelow(params Point3D[] points){
		foreach (Point3D p in points) {
			if (this.y>p.y) return false;
		} return true;
	}

	// Properties --------------------------------------

	public Point2D xy{ get{ return new Point2D (x, y); } }
	public Point2D yx{ get{ return new Point2D (y, x); } }
	public Point2D yz{ get{ return new Point2D (y, z); } }
	public Point2D zy{ get{ return new Point2D (z, y); } }
	public Point2D zx{ get{ return new Point2D (z, x); } }
	public Point2D xz{ get{ return new Point2D (y, z); } }

	// Static ------------------------------------------

	public static Point3D GetCenter(IEnumerable<Point3D> e){

		Vector3D C = Vector3D.zero;
		int n = 0;
		foreach (Point3D P in e) {
			C += P;
			n++;
		} return (C / n).asPoint;

	}
	
	// Operators --------------------------------------------

	public static Point3D operator +(Point3D A, Vector3D u) {
		return new Point3D(A.x+u.x, A.y+u.y, A.z+u.z);
	}

	public static Vector3D operator -(Point3D A, Point3D B) {
		return new Vector3D(A.x-B.x, A.y-B.y, A.z-B.z);
	}

	public static Vector3D operator -(Point3D A, Vector3D u) {
		return new Vector3D(A.x-u.x, A.y-u.y, A.z-u.z);
	}
	
	public static Point3D operator /(Point3D A, Point3D B) {
		return new Point3D(A.x/B.x, A.y/B.y, A.z/B.z);
	}
	
	public static Point3D operator *(Point3D A, Point3D B) {
		return new Point3D(A.x*B.x, A.y*B.y, A.z*B.z);
	}
	
	// -----------------------------------------------------
	
	public static Point3D operator +(Point3D P, float x) {
		return new Point3D(P.x+x, P.y+x, P.z+x);
	}
	
	public static Point3D operator -(Point3D P, float x) {
		return new Point3D(P.x-x, P.y-x, P.z-x);
	}
	
	public static Point3D operator *(Point3D P, float x) {
		return new Point3D(P.x*x, P.y*x, P.z*x);
	}
	
	public static Point3D operator /(Point3D P, float x) {
		return new Point3D(P.x/x, P.y/x, P.z/x);
	}

}
