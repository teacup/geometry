﻿using UnityEngine;
using System.Collections;

public class Segment3D : Shape3D {

	private Point3D[] ends;

	public Segment3D(Point3D A, Point3D B){

		ends = new Point3D[]{ A, B };

	}

	// Getting information ---------------

	public Point3D this[int i]{ get{ return ends [i]; } }

	public Point3D A{get{ return ends [0];}}
	public Point3D B{get{ return ends [1];}}

	public float Min(int dim){ return A.Get(dim) < B.Get(dim) ? A.Get(dim) : B.x; }
	public float Max(int dim){ return A.Get(dim) > B.Get(dim) ? A.Get(dim) : B.x; }

}
