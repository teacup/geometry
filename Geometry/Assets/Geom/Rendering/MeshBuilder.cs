﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBuilder{

	List<Vector3> coord = new List<Vector3>();
	List<Vector3> normals = new List<Vector3>();
	List<int> index = new List<int>();

	public void AddAllTriangles (List<Polygon2D> gons, Transform target){ 
		foreach (var t in gons) AddXZTriangle (t.corners, target); 
	}

	public void AddXZTriangle(List<Vector2> corners, Transform target){
		var a = new Vector3 (corners[0].x, 0f, corners[0].y);
		var b = new Vector3 (corners[1].x, 0f, corners[1].y);
		var c = new Vector3 (corners[2].x, 0f, corners[2].y);
		if (target) {
			a = target.InverseTransformPoint (a);
			b = target.InverseTransformPoint (b);
			c = target.InverseTransformPoint (c);
		}
		AddTriangle (a, b, c);
	}

	public void AddTriangle(Vector3 a, Vector3 b, Vector3 c){
		coord.Add (a); coord.Add (b); coord.Add (c);
		Vector3 u = b - a, v = c - a;
		Vector3 w = Vector3.Cross (u, v).normalized;
		for (int i = 0; i < 3; i++){
			normals.Add (w); 
			index.Add (index.Count);
		}
	}

	public void Clear(){
		coord = new List<Vector3>();
		normals = new List<Vector3>();
		index = new List<int>();
	}

	public UnityEngine.Mesh mesh{
		get{ 
			var m = new Mesh ();
			m.vertices = coord.ToArray ();
			m.normals = normals.ToArray ();
			m.triangles = index.ToArray();
			return m;
		}
	}

}
