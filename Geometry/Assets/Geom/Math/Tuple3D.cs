﻿using UnityEngine;
using System.Collections;

public class Tuple3D {

	protected float[] coordinates;
	
	// constructors -----------------------------------------
	
	public Tuple3D (float x, float y, float z) {
		
		coordinates = new float[]{ x, y, z };
		
	}

	// getting information ---------------------------------
	
	public float x{ 
		get{return coordinates[0];} 
		set{ coordinates [0]=value; }
	}
	
	public float y{ 
		get{return coordinates[1];} 
		set{ coordinates [1]=value; }
	}
	
	public float z{ 
		get{ return coordinates [2]; }
		set{ coordinates [2]=value; }
	} 

	// operators --------------------------------------------

	public float Get(int index){return coordinates[index];}

}
