﻿using UnityEngine;

public class Quaternion3D {

	public float x, y, z, w;

	public Quaternion3D(float x, float y, float z, float w){

		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;

	}

	public Quaternion3D(float angleInRadians, Vector3D axis){

		float h = angleInRadians * 0.5f;
		float sin = Mathf.Sin(h);
		float cos = Mathf.Cos(h);
		x = axis.x * sin;
		y = axis.y * sin;
		z = axis.z * sin;
		w = cos;

	}

	// operators ----------------------------------------------------

	/**
	 * Note: rotates a point around the origin; formula is same as quaternion multiplication
	 * by promoting q2 to a quaternion with w=0f
	 */
	public static Point3D operator *(Quaternion3D q1, Point3D q2){
		
		float x = (q1.y * q2.z) - (q1.z * q2.y);
		float y = (q1.z * q2.x) - (q1.x * q2.z);
		float z = (q1.x * q2.y) - (q1.y * q2.x);
		return new Point3D (
			q2.x * q1.w + x,
			q2.y * q1.w + y,
			q2.z * q1.w + z );
		
	}

	/** 
	 * Note: rotates a vector; formula is same as quaternion multiplication
	 * by promoting q2 to a quaternion with w=0f
	 */
	public static Vector3D operator *(Quaternion3D q1, Vector3D q2){

		float x = (q1.y * q2.z) - (q1.z * q2.y);
		float y = (q1.z * q2.x) - (q1.x * q2.z);
		float z = (q1.x * q2.y) - (q1.y * q2.x);
		return new Vector3D (
			q2.x * q1.w + x,
			q2.y * q1.w + y,
			q2.z * q1.w + z );

	}

	public static Quaternion3D operator *(Quaternion3D q1, Quaternion3D q2){

		float x = (q1.y * q2.z) - (q1.z * q2.y);
		float y = (q1.z * q2.x) - (q1.x * q2.z);
		float z = (q1.x * q2.y) - (q1.y * q2.x);
		float w = ((q1.x * q2.x) + (q1.y * q2.y)) + (q1.z * q2.z);
		return new Quaternion3D (
			((q1.x * q2.w) + (q2.x * q1.w)) + x,
			((q1.y * q2.w) + (q2.y * q1.w)) + y,
			((q1.z * q2.w) + (q2.z * q1.w)) + z,
			(q1.w * q2.w) - w
			);

	}

}
