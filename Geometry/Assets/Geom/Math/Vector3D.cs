﻿using UnityEngine;
using System.Collections;

public class Vector3D : Tuple3D {

	public static Vector3D zero	   = new Vector3D(0f, 0f, 0f);
	public static Vector3D right   = new Vector3D(1f, 0f, 0f);
	public static Vector3D up	   = new Vector3D(0f, 1f, 0f);
	public static Vector3D forward = new Vector3D(0f, 0f, 1f);

	// Constructors ------------------------------------------------------------------

	public Vector3D (float x, float y, float z) : base(x,y,z) {}

	public void Normalize(){
		float l = length;
		if(l==0f){ throw new System.Exception("Zero vector cannot be normalized"); }
		Scale (1f / length);
	}

	public void Scale(float x){
		for(int i=0;i<3;i++)coordinates[i]*=x;
	}
	
	// operations -------------------------------------------

	public float Dot(Vector3D that){ return Vector3D.Dot (this, that); }

	public Vector3D Cross(Vector3D that){ return Vector3D.Cross (this, that); }

	public Quaternion3D RotationTo(Vector3D that){
		
		Vector3D axis = Cross(that).normalized;
		float angle = Mathf.Acos(Dot(that));
		return new Quaternion3D(angle, axis);
		
	}

	// properties -------------------------------------------

	public Point3D asPoint{ get{ return new Point3D(x, y, z); } }

	public float length{
		get{
			return Mathf.Sqrt (x*x+y*y+z*z);
		}
		set{
			float l = length;
			if(l==0f){ throw new System.Exception("Zero vector cannot be adjusted"); }
			Scale (length/l);
		}
	}

	public Vector3D normalized{
		get{
			Vector3D u = new Vector3D (x, y, z);
			u.Normalize ();
			return u;
		}
	}

	// static -----------------------------------------------

	public static float Dot(Vector3D u, Vector3D v){

		return (u.x * v.x) + (u.y * v.y) + (u.z * v.z);

	}

	public static Vector3D Cross(Vector3D u, Vector3D v){

		return new Vector3D(
			u.y * v.z - v.y * u.z,
			u.z * v.x - v.z * u.x,
			u.x * v.y - v.x * u.y);

	}

	// operators --------------------------------------------
	
	public static Vector3D operator +(Vector3D A, Vector3D B) {
		return new Vector3D(A.x+B.x, A.y+B.y, A.z+B.z);
	}

	public static Vector3D operator +(Vector3D A, Point3D B) {
		return new Vector3D(A.x+B.x, A.y+B.y, A.z+B.z);
	}

	public static Vector3D operator -(Vector3D A, Vector3D B) {
		return new Vector3D(A.x-B.x, A.y-B.y, A.z-B.z);
	}
	
	public static Vector3D operator /(Vector3D A, Vector3D B) {
		return new Vector3D(A.x/B.x, A.y/B.y, A.z/B.z);
	}
	
	public static Vector3D operator *(Vector3D A, Vector3D B) {
		return new Vector3D(A.x*B.x, A.y*B.y, A.z*B.z);
	}

	public static Vector3D operator *(Vector3D u, Quaternion3D q) {
		Vector3D q1 = q * u;
		return new Vector3D (q1.x, q1.y, q1.z);
	}

	// -----------------------------------------------------
	
	public static Vector3D operator +(Vector3D P, float x) {
		return new Vector3D(P.x+x, P.y+x, P.z+x);
	}
	
	public static Vector3D operator -(Vector3D P, float x) {
		return new Vector3D(P.x-x, P.y-x, P.z-x);
	}
	
	public static Vector3D operator *(Vector3D P, float x) {
		return new Vector3D(P.x*x, P.y*x, P.z*x);
	}
	
	public static Vector3D operator /(Vector3D P, float x) {
		return new Vector3D(P.x/x, P.y/x, P.z/x);
	}

}
