﻿using UnityEngine;
using System.Collections;

public class Vector2D {
	
	public static Vector2D zero	   = new Vector2D(0f, 0f);
	public static Vector2D right   = new Vector2D(1f, 0f);
	public static Vector2D up	   = new Vector2D(0f, 1f);

	protected float[] coordinates;
	
	// Constructors ------------------------------------------------------------------
	
	public Vector2D (float x, float y) {
		this.x=x;
		this.y=y;
	}
	
	public void Normalize(){
		float l = length;
		if(l==0f){ throw new System.Exception("Zero vector cannot be normalized"); }
		Scale (1f / length);
	}
	
	public void Scale(float s){
		for(int i=0;i<2;i++)coordinates[i]*=s;
	}
	
	// operations -------------------------------------------
	
	public float Dot(Vector2D that){ return Vector2D.Dot (this, that); }
	
	// properties -------------------------------------------
	
	public Point2D point{ get{ return new Point2D(x, y); } }
	
	public float length{
		get{
			return Mathf.Sqrt (x*x+y*y);
		}
		set{
			float l = length;
			if(l==0f){ throw new System.Exception("Zero vector cannot be adjusted"); }
			Scale (length/l);
		}
	}
	
	public Vector2D normalized{
		get{
			Vector2D u = new Vector2D (x, y);
			u.Normalize ();
			return u;
		}
	}

	public float x{ 
		get{return coordinates[0];} 
		set{ coordinates [0]=value; }
	}
	
	public float y{ 
		get{return coordinates[1];} 
		set{ coordinates [1]=value; }
	}

	// static -----------------------------------------------
	
	public static float Dot(Vector2D u, Vector2D v){
		
		return (u.x * v.x) + (u.y * v.y);
		
	}
	
	// operators --------------------------------------------
	
	public static Vector2D operator +(Vector2D A, Vector2D B) {
		return new Vector2D(A.x+B.x, A.y+B.y);
	}
	
	public static Vector2D operator +(Vector2D A, Point2D B) {
		return new Vector2D(A.x+B.x, A.y+B.y);
	}
	
	public static Vector2D operator -(Vector2D A, Vector2D B) {
		return new Vector2D(A.x-B.x, A.y-B.y);
	}
	
	public static Vector2D operator /(Vector2D A, Vector2D B) {
		return new Vector2D(A.x/B.x, A.y/B.y);
	}
	
	public static Vector2D operator *(Vector2D A, Vector2D B) {
		return new Vector2D(A.x*B.x, A.y*B.y);
	}
	
	// -----------------------------------------------------
	
	public static Vector2D operator +(Vector2D P, float x) {
		return new Vector2D(P.x+x, P.y+x);
	}
	
	public static Vector2D operator -(Vector2D P, float x) {
		return new Vector2D(P.x-x, P.y-x);
	}
	
	public static Vector2D operator *(Vector2D P, float x) {
		return new Vector2D(P.x*x, P.y*x);
	}
	
	public static Vector2D operator /(Vector2D P, float x) {
		return new Vector2D(P.x/x, P.y/x);
	}
	
}