# README #

A repository containing small Unity projects. This works as an incubator
and share small experiments which don't fit in a 'code snippet' (a la gist) format.

### Purpose ###

Currently, this contains a 2D geometry package and demo project. A neat feature is that you can build and test geometry in real time inside the Unity Editor.

2D Primitives: Line, Point, Polygon, Ray, Segment 

3D Primitives: Mesh, Point, Polygon, Segment

Math primitives: Tuple, Vector, Quaternion

Supported operations: Intersections, Containment tests, triangulation (in progress)

### How do I get set up? ###

Unity is required.

### Who do I talk to? ###

Contact tea.desouza [at] gmail [dot] com for comments or queries.