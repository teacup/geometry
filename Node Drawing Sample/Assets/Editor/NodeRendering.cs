﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class NodeRendering : EditorWindow{
	
	// Add menu item named "Node Editor" to the Window menu
	[MenuItem("Window/Node Rendering")]
	public static void ShowWindow(){

		// Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(NodeRendering));

	}
	
	void OnGUI(){
		
		Vector2 P = Vector2.one*50f;
		Vector2 Q = Vector2.one*100f;

		Handles.color = Color.black;
		Handles.BeginGUI( );
		Handles.DrawLine ( P, Q );
		Handles.EndGUI();

		// Handles.DrawBezier(
		//	new Vector3(100f,100f),
		//	new Vector3(200f,200f),
		//
		//	);

		Vector2 size = new Vector2( 64f, 32f );
		Rect rectA = new Rect (P-size/2, size);
		Rect rectB = new Rect (Q-size/2, size);
		GUI.Box(rectA, "NODE A");
		GUI.Box(rectB, "NODE B");
		
	}

}
