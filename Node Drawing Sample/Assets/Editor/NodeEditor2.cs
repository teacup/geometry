﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class NodeEditor2 : EditorWindow{

	Node node1;
	Node node2;
	Node dragging;

	public NodeEditor2(){
		Debug.Log ("Editor created");
		wantsMouseMove = true;
		node1 = new Node ("FOO", Vector2.one * 50f);
		node2 = new Node ("BAR", Vector2.one * 100f);
	}

	// Add menu item named "Node Editor" to the Window menu
	[MenuItem("Window/Node Editor 2")]
	public static void ShowWindow(){

		// Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(NodeEditor2));

	}
	
	void OnGUI(){

		GUILayout.Label("Button: " + Event.current.button);
		GUILayout.Label("Mouse: " + Event.current.mousePosition);

		Handles.color = Color.black;
		Handles.BeginGUI( );
		Handles.DrawLine ( node1.position, node2.position );
		Handles.EndGUI();

		Vector2 size = new Vector2( 64f, 32f );
		Rect rectA = new Rect (node1.position - size/2, size);
		Rect rectB = new Rect (node2.position - size/2, size);
		GUI.Box(rectA, node1.name);
		GUI.Box(rectB, node2.name);

		switch (Event.current.type) {
		case EventType.MouseMove:
			Repaint ();
			break;
		case EventType.MouseDown:
			OnMouseDown ();
			break;	
		case EventType.MouseDrag:
			OnMouseDrag ();
			Repaint ();
			break;
		case EventType.MouseUp:
			dragging = null;
			break;
		}
	}

	void OnMouseDrag(){
		if (dragging == null) return;
		dragging.position = Event.current.mousePosition;
	}

	void OnMouseDown(){
		if (Inside (node1)) {
			dragging = node1;
			Debug.Log ("Inside node1");
		}
		if (Inside (node2)) {
			dragging = node2;
			Debug.Log ("Inside node2");
		}
	}

	bool Inside(Node node){
		Vector2 pos = Event.current.mousePosition;
		return Vector2.Distance (pos, node.position) < 30f;
	}

	class Node{

		public string name;
		public Vector2 position;
		public Node(string name, Vector2 position){
			this.name = name;
			this.position= position;
		}

	}

}
