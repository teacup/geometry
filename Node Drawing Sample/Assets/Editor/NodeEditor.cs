﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class NodeEditor : EditorWindow{

	Vector2 node1;
	Vector2 node2;

	public NodeEditor(){
		Debug.Log ("Editor created");
		wantsMouseMove = true;
		node1 = Vector2.one*50f;
		node2 = Vector2.one*100f;
	}

	// Add menu item named "Node Editor" to the Window menu
	[MenuItem("Window/Node Editor")]
	public static void ShowWindow(){

		// Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(NodeEditor));

	}
	
	void OnGUI(){

		GUILayout.Label("Button: " + Event.current.button);
		GUILayout.Label("Mouse: " + Event.current.mousePosition);

		Handles.color = Color.black;
		Handles.BeginGUI( );
		Handles.DrawLine ( node1, node2 );
		Handles.EndGUI();

		// Handles.DrawBezier(
		//	new Vector3(100f,100f),
		//	new Vector3(200f,200f),
		//
		//	);

		Vector2 size = new Vector2( 64f, 32f );
		Rect rectA = new Rect (node1-size/2, size);
		Rect rectB = new Rect (node2-size/2, size);
		GUI.Box(rectA, "NODE A");
		GUI.Box(rectB, "NODE B");

		if (Event.current.type == EventType.MouseMove) Repaint ();
	
		if (Event.current.type == EventType.MouseDown) {
			OnMouseDown ();
		}

	}

	void OnMouseDown(){

		if (Inside (node1)) {
			Debug.Log ("Inside node1");
		}
		if (Inside (node2)) {
			Debug.Log ("Inside node2");
		}

	}

	bool Inside(Vector2 coord){

		Vector2 pos = Event.current.mousePosition;
		return Vector2.Distance (pos, coord) < 30f;

	}


}
